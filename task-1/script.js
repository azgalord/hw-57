const tasks = [
  {id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},
  {id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},
  {id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},
  {id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},
  {id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},
  {id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},
  {id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'},
];

const getTimeOfFront = (category) => {
  let fullTime = 0;

  const frontend = tasks.filter(object => object.category === category);

  frontend.map(task => {
    fullTime += task.timeSpent;
  });

  console.log('Time spent for ' + category + ' tasks: ' + fullTime);
};

const reducer = (accumulator, currentValue) => accumulator + currentValue;

const getTimeOfBugs = (type) => {
  const array = [];
  const bugs = tasks.filter(object => object.type === type);

  bugs.map(task => {
    array.push(task.timeSpent);
  });

  console.log('Time spent for bugs: ' + array.reduce(reducer));
};

const findUi = () => {
  const count = tasks.filter(object => (object.title.indexOf('UI') > -1));
  console.log('Count of UI tasks: ' + count.length);
};

const getAllTasks = () => {
  const frontTasks = tasks.filter(obj => obj.category === 'Frontend');
  const backTasks = tasks.filter(obj => obj.category === 'Backend');

  const tasksObject = {
    Frontend: frontTasks.length,
    Backend: backTasks.length
  };

  console.log(tasksObject);
};

const getTasksOverForHours = () => {
  const newTasks = tasks.filter(obj => obj.timeSpent > 4);
  newTasks.map(task => {
    delete task.id;
    delete task.timeSpent;
    delete task.type;
  });
  console.log(newTasks);
};

getTimeOfFront('Frontend');
getTimeOfBugs('bug');
findUi();
getAllTasks();
getTasksOverForHours();
