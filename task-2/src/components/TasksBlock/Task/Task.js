import React from 'react';
import './Task.css';
import Icon from '../../../assets/img/times-solid.svg';

const Task = props => (
    <div className="Task">
      <h2>{props.title}</h2>
      <span>{props.summ} KGS</span>
      <button onClick={props.onClick} type="button"><img src={Icon} alt=""/></button>
    </div>
);

export default Task;
