import React from 'react';
import './TotalSpent.css';

const TotalSpent = ({spent}) => (
    <div className="TotalSpent">
      <h1>Total spent:</h1>
      <p>{spent} KGS</p>
    </div>
);

export default TotalSpent;
