import React from 'react';
import Task from "./Task/Task";
import './TaskBlock.css';
import TotalSpent from "./TotalSpent/TotalSpent";

const TaskBlock = props => (
    <div className="TaskBlock">
      {props.tasks.map((element, index) => (
          <Task
              onClick={event => props.deleteElement(event, index)}
              title={element.name}
              summ={element.cost}
              key={index}/>
      ))}
      <TotalSpent spent={props.spent}/>
    </div>
);

export default TaskBlock;
