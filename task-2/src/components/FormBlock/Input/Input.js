import React from 'react';
import './Input.css';

const Input = props => (
    <input
        className="Input"
        type="text" placeholder="Item name"
        onChange={props.onChange}
        value={props.value}
    />
);

export default Input;
