import React from 'react';
import Input from "./Input/Input";
import Cost from "./Cost/Cost";
import AddButton from "./AddButton/AddButton";

import './FormBlock.css';

const FormBlock = props => (
    <div className="FormBlock">
      <Input
          value={props.value}
          onChange={event => props.changeInput(event)}
      />
      <Cost
          value={props.valueOfCost}
          onChange={event => props.changeCost(event)}
      />
      <AddButton onClick={event => props.clickButton(event)}/>
    </div>
);

export default FormBlock;
