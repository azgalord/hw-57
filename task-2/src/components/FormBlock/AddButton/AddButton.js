import React from 'react';
import './AddButton.css';

const AddButton = props => (
    <button className="AddButton" onClick={props.onClick} type="button">Add</button>
);

export default AddButton;
