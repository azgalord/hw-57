import React from 'react';
import './Cost.css';

const Cost = props => (
    <div>
      <input
          className="Cost"
          type="text"
          placeholder="Cost"
          value={props.value}
          onChange={props.onChange}
      />
      <span className="CostKGS">KGS</span>
    </div>
);

export default Cost;
