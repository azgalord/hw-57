import React, {Component} from 'react';
import FormBlock from "../../components/FormBlock/FormBlock";
import TaskBlock from "../../components/TasksBlock/TaskBlock";

import './App.css';

class App extends Component {
  state = {
    items: [],
    inputText: '',
    costText: ''
  };

  changeInput = (event) => {
    const inputText = event.target.value;
    this.setState({inputText});
  };

  changeCost = (event) => {
    const costText = event.target.value;
    this.setState({costText});
  };

  clickButton = (event) => {
    event.preventDefault();
    if (this.state.inputText !== '' && this.state.costText !== '' && !isNaN(this.state.costText)) {
      const items = [...this.state.items];
      const item = {
        name: this.state.inputText,
        cost: this.state.costText
      };
      items.push(item);

      this.setState({items, inputText: '', costText: ''});
    } else {
      alert('Inputs are empty, or "cost" is not numbers!');
    }
  };

  deleteElement = (event, index) => {
    event.preventDefault();

    const items = [...this.state.items];
    items.splice(index, 1);
    this.setState({items});
  };

  spentCount = () => {
    const costs = [];
    let reduced = (accum, currentVal) => accum + currentVal;

    if (this.state.items.length) {
      this.state.items.map(el => {
        costs.push(parseInt(el.cost));
      });
      return costs.reduce(reduced);
    }
  };

  render() {
    return (
        <div className="App">
          <FormBlock
              value={this.state.inputText}
              valueOfCost={this.state.costText}
              changeInput={this.changeInput}
              changeCost={this.changeCost}
              clickButton={this.clickButton}
          />
          <TaskBlock
              tasks={this.state.items}
              deleteElement={this.deleteElement}
              spent={this.spentCount()}
          />
        </div>
    );
  }
}

export default App;
