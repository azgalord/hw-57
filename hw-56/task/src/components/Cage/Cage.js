import React from 'react';
import './Cage.css';

const Cage = props => (
    <div onClick={props.onClick} className={props.classname}>
        {props.content}
    </div>
);

export default Cage;
