import React from 'react';
import './Counter.css';

const Counter = props => (
    <p className="Counter">Tries: {props.count}</p>
);

export default Counter;
