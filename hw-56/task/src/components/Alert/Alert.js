import React from 'react';
import './Alert.css';

const Alert = props => (
    <div className="AlertContainer">
        <div className="Alert">
            <h2>Congratulations!</h2>
            <p>You won the game, with a score: {props.score}</p>
            <button onClick={props.onClick}>Ok!</button>
        </div>
    </div>
);

export default Alert;
