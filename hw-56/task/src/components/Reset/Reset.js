import React from 'react';
import './Reset.css';

const Reset = props => (
    <button className="Reset" onClick={props.onClick}>Reset</button>
);

export default Reset;
