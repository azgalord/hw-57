import React from 'react';
import './Board.css';

const Board = props => (
    <div className="Board">
        {props.elements}
    </div>
);

export default Board;
