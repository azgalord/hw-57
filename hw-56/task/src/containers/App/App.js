import React, {Component} from 'react';
import Board from '../../components/Board/Board';
import Cage from "../../components/Cage/Cage";
import Reset from "../../components/Reset/Reset";
import Counter from "../../components/Counter/Counter";
import Alert from "../../components/Alert/Alert";

import './App.css';

class App extends Component {
    state = {
        cagesArray: [],
        count: 0,
        ifFound: false,
        alert: false
    };

    GetInfoToCagesArray = () => {
        const cagesArray = [];
        for (let i = 0; i < 36; i++) {
            cagesArray.push(0);
        }
        const random = Math.floor(Math.random() * 36);
        cagesArray.splice(random, 1, 1);
        this.setState({cagesArray, count: 0, isFound: false, alert: false});
    };

    findRing = (index) => {
        let count = this.state.count;
        const cagesArray = [...this.state.cagesArray];
        if (cagesArray[index] === 1) {
            cagesArray[index] = 3;
            this.setState({isFound: true, alert: true})
        }
        if (cagesArray[index] === 0){
            cagesArray[index] = 2;
        }
        count++;
        this.setState({cagesArray, count});
    };

    componentDidMount () {
        this.GetInfoToCagesArray();
    }

    Alert = () => (
        <Alert
            score={this.state.count}
            onClick={() => (this.setState({alert: false}))}
        />
    );

    render() {
        return (
            <div className="App">
                {this.state.alert ? this.Alert() : null}
                <Board
                    elements={this.state.cagesArray.map((el, index) => (
                        <Cage
                            key={index}
                            classname={(el === 2 || el === 3) ? 'Cage CageWhite' : 'Cage'}
                            onClick={(!this.state.isFound && el !== 2) ? (() => this.findRing(index)) : null}
                            content={(el === 3) ? 'O' : ''}
                        />
                    ))}
                />
                <Counter count={this.state.count}/>
                <Reset onClick={this.GetInfoToCagesArray}/>
            </div>
        );
    }
}

export default App;
